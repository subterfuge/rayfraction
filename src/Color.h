#pragma once

#include "raylib_namespace.h"
#include "imgui.h"

namespace Color
{
    constexpr rl::Color White = { 255, 255, 255, 255 };
    constexpr rl::Color Blue = { 0, 221, 255, 255 };
    constexpr rl::Color Red = { 255, 0, 98, 255 };
    constexpr rl::Color Green = { 3, 252, 111, 255 };
    constexpr rl::Color Orange = { 255, 152, 0, 255 };
    constexpr rl::Color Background = { 34, 26, 26, 228 }; //{ 43, 54, 57, 228 };
    constexpr rl::Color LightRed = { 255, 247, 247, 255 };
    constexpr rl::Color Transparent = { 0, 0, 0, 0 };
    constexpr rl::Color DarkRed = { 255, 0, 0, 150 };

    constexpr int PaletteSize = 7;

    constexpr rl::Color Palette[PaletteSize] = { White, Blue, Red, Green, Orange, Background, Transparent };

#if (DEBUG_LEVEL >= 2)

    constexpr rl::Color DebugBlue = Blue;
    constexpr rl::Color DebugRed = Red;
    constexpr rl::Color DebugGreen = Green;

    inline ImVec4 RlToIm(const rl::Color& color)
    {
        return ImVec4 {
                static_cast<float>(color.r) / 255.f,
                static_cast<float>(color.g) / 255.f,
                static_cast<float>(color.b) / 255.f,
                static_cast<float>(color.a) / 255.f,
        };
    }

    inline rl::Color ImToRl(const ImVec4& color)
    {
        return rl::Color {
                static_cast<unsigned char>(color.x * 255.f),
                static_cast<unsigned char>(color.y * 255.f),
                static_cast<unsigned char>(color.z * 255.f),
                static_cast<unsigned char>(color.w * 255.f),
        };
    }

#endif
}