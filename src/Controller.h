#pragma once

#include "Debug.h"

class IControllerState
{
public:
    IControllerState() = default;
    virtual ~IControllerState() = default;

    [[nodiscard]] virtual IControllerState* Clone() const = 0;

#ifdef DEBUG_TOOLS
    virtual void DebugMenu() = 0;
    virtual void DebugFps() = 0;
    virtual void DebugEntityList() = 0;
    virtual void DebugEntityExplorer() = 0;
    virtual void DebugControls() = 0;
    virtual void DebugImGuiDemo() = 0;
#endif
};

template<class Derived>
class ControllerState : public IControllerState
{
public:
    ControllerState() = default;
    ControllerState(const ControllerState& state) = default;
    ~ControllerState() override = default;

    [[nodiscard]] IControllerState* Clone() const override
    {
        return new Derived(static_cast<const Derived&>(*this));
    }

#ifdef DEBUG_TOOLS

    void DebugMenu() override
    {
        Debug::SwitchMainMenu();
    }

    void DebugFps() override
    {
        Debug::SwitchFpsDisplay();
    }

    void DebugEntityList() override
    {
        Debug::SwitchEntityList();
    }

    void DebugEntityExplorer() override
    {
        Debug::SwitchEntityExplorer();
    }

    void DebugControls() override
    {
        Debug::SwitchControls();
    }

    void DebugImGuiDemo() override
    {
        Debug::SwitchImGuiDemo();
        Debug::SwitchImPlotDemo();
    }

#endif
};

class BaseControllerState : public ControllerState<BaseControllerState>
{};

class ControllerSystem
{
private:
    IControllerState* state_;

public:
    ControllerSystem();
    ControllerSystem(const ControllerSystem& controller);
    ControllerSystem(ControllerSystem&& controller) noexcept;
    ControllerSystem& operator=(const ControllerSystem& controller);
    ControllerSystem& operator=(ControllerSystem&& controller) noexcept;
    ~ControllerSystem();

    void Update();

    void SwitchState(IControllerState* newState);
};
