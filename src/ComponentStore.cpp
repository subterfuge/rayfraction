#include "ComponentStore.h"

#include <algorithm>
#include "Component.h"

namespace ComponentStore
{
    namespace
    {
        ComponentId currentComponentId = COMPONENT_ID_NULL;
    }

    void Store(const ComponentId& componentTypeId, Component* component)
    {
        ComponentTableBase& table = GetDatabase()[componentTypeId];
        table[component->GetEntityId()].push_back(component);
    }

    std::map<ComponentId, ComponentTableBase>& GetDatabase()
    {
        static std::map<ComponentId, ComponentTableBase> componentDatabase;
        return componentDatabase;
    }

    std::vector<Component*> AllInEntity(const EntityId& entity)
    {
        std::vector<Component*> result;
        for(auto it : GetDatabase())
        {
            auto entityIt = it.second.find(entity);
            if(entityIt == it.second.end())
                continue;

            for(Component* component : entityIt->second)
                result.push_back(component);
        }
        return result;
    }

    ComponentId NextId()
    {
        return ++currentComponentId;
    }

    void DeleteAll(EntityId entity)
    {
        for(auto it : GetDatabase())
        {
            auto entityIt = it.second.find(entity);
            if(entityIt == it.second.end())
                continue;

            for(Component* component : entityIt->second)
                component->PendingDelete();
        }
    }

    // todo: change that, not optimal
    void EndDelete(EntityId entity, Component* component)
    {
        for(int i = 0; i < GetDatabase().size(); ++i)
        {
            auto entityIt = GetDatabase()[i].find(entity);
            if(entityIt == GetDatabase()[i].end())
                continue;

            auto compIt = std::find(entityIt->second.begin(), entityIt->second.end(), component);
            if(compIt != entityIt->second.end())
            {
                delete component;
                std::vector<Component*>& comps = GetDatabase()[i][entity];
                comps.erase(compIt);
                if(comps.size() == 0)
                    GetDatabase()[i].erase(entity);
                return;
            }
        }
    }
}
