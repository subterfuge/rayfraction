#pragma once

#include <vector>
#include "Entity.h"

class IScene
{
public:
    virtual ~IScene() = default;

    virtual void Init() = 0;
    virtual void Draw() = 0;
    virtual void Update(const double& dt) = 0;
    virtual IScene* SwitchScene() = 0;
#ifdef DEBUG_TOOLS
    virtual void DebugDraw() = 0;
#endif
};

class Scene : public IScene
{
public:
    ~Scene() override = default;

    void Init() override;

    void Draw() override;

    void Update(const double& dt) override;

    IScene* SwitchScene() override;

#ifdef DEBUG_TOOLS

    void DebugDraw() override;

#endif
};
