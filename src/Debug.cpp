#ifdef DEBUG_TOOLS

#include "Debug.h"

#include "implot.h"

void Debug::Draw()
{
    if(Display::MAIN_MENU == (display_ & Display::MAIN_MENU))
        ShowMainMenu();

    if(Display::FPS_COUNTER == (display_ & Display::FPS_COUNTER))
        ShowFpsCounter();

    if(Display::ENTITY_LIST == (display_ & Display::ENTITY_LIST))
        ShowEntityList();

    if(Display::ENTITY_EXPLORER == (display_ & Display::ENTITY_EXPLORER))
        ShowEntityExplorer();

    if(Display::CONTROLS == (display_ & Display::CONTROLS))
        ShowControls();

    if(Display::IMGUI_DEMO == (display_ & Display::IMGUI_DEMO))
    {
        bool open = true;
        ImGui::ShowDemoWindow(&open);
        if(!open)
            SwitchImGuiDemo();
    }

    if(Display::IMPLOT_DEMO == (display_ & Display::IMPLOT_DEMO))
    {
        bool open = true;
        ImPlot::ShowDemoWindow(&open);
        if(!open)
            SwitchImPlotDemo();
    }

    if(colorPickerOpened_)
        ImGui::OpenPopup("DefaultColorPicker");
    colorPickerOpened_ = false;

    if(ImGui::BeginPopup("DefaultColorPicker"))
    {
        DrawColorPicker();
        ImGui::EndPopup();
    }

    for(auto* it : drawList_)
    {
        it->DebugDraw();
    }
}

void Debug::SwitchFpsDisplay()
{
    display_ ^= Display::FPS_COUNTER;
}

void Debug::SwitchMainMenu()
{
    display_ ^= Display::MAIN_MENU;
}

void Debug::SwitchEntityExplorer()
{
    display_ ^= Display::ENTITY_EXPLORER;
}

void Debug::SwitchEntityList()
{
    display_ ^= Display::ENTITY_LIST;
}

void Debug::SwitchControls()
{
    display_ ^= Display::CONTROLS;
}

void Debug::SwitchImGuiDemo()
{
    display_ ^= Display::IMGUI_DEMO;
}

void Debug::SwitchImPlotDemo()
{
    display_ ^= Display::IMPLOT_DEMO;
}

void Debug::SelectEntity(const EntityId& entity)
{
    selectedEntity_ = entity;
}

void Debug::DisplayColorPicker(rl::Color* colorToUpdate)
{
    updatedColor_ = colorToUpdate;
    colorPickerOpened_ = true;
}

void Debug::AddToDrawList(Component* const component)
{
    drawList_.push_back(component);
}

void Debug::RemoveFromDrawList(Component* const component)
{
    auto it = std::find(drawList_.begin(), drawList_.end(), component);
    drawList_.erase(it);
}

bool Debug::GetPaused()
{
    return paused_;
}

unsigned int Debug::GetStep()
{
    return step_;
}

void Debug::DecrementStep()
{
    step_ = std::max(0, static_cast<int>(step_) - 1);
}

void Debug::ShowMainMenu()
{
    if(ImGui::BeginMainMenuBar())
    {
        if(ImGui::BeginMenu("View"))
        {
            if(ImGui::MenuItem("FPS counter", "F1"))
                display_ ^= Display::FPS_COUNTER;
            if(ImGui::MenuItem("Entity list", "F2"))
                display_ ^= Display::ENTITY_LIST;
            if(ImGui::MenuItem("Entity explorer", "F3"))
                display_ ^= Display::ENTITY_EXPLORER;
            if(ImGui::MenuItem("Controls", "F4"))
                display_ ^= Display::CONTROLS;
            if(ImGui::MenuItem("ImGui demo window", "F5"))
            {
                display_ ^= Display::IMGUI_DEMO;
                display_ ^= Display::IMPLOT_DEMO;
            }
            ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }
}

void Debug::ShowFpsCounter()
{
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize |
                                    ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;
    ImGui::SetNextWindowBgAlpha(0.35f); // Transparent background
    bool open = true;
    if (ImGui::Begin("Overlay", &open, window_flags))
    {
        ImGui::Text("FPS: %i", rl::GetFPS());
    }
    ImGui::End();
}

void Debug::ShowEntityExplorer()
{
    bool open = true;
    if(ImGui::Begin("Entity explorer", &open))
    {
        ImGui::PushItemWidth(80.f);
        if(selectedEntity_ != ENTITY_ID_NULL)
        {
            auto componentList = ComponentStore::AllInEntity(selectedEntity_);
            for(auto it : componentList)
            {
                it->DebugGui();
            }
        }
        ImGui::PopItemWidth();
    }
    ImGui::End();
    if(!open)
        SwitchEntityExplorer();
}

void Debug::ShowEntityList()
{
    bool open = true;
    if(ImGui::Begin("Entity list", &open))
    {
        int i = 0;
        ComponentStore::ForEach<CDebugInfo>([&](CDebugInfo* debugInfo) {
            if (ImGui::Selectable(debugInfo->GetName().c_str(),
                                  selectedEntityIndex_ == i))
            {
                selectedEntityIndex_ = i;
                selectedEntity_ = debugInfo->GetEntityId();
            }
            ++i;
        });
    }
    ImGui::End();
    if(!open)
        SwitchEntityList();
}

void Debug::ShowControls()
{
    bool open = true;
    if(ImGui::Begin("Controls", &open, ImGuiWindowFlags_AlwaysAutoResize))
    {
        std::string pauseLbl;
        if(paused_)
            pauseLbl = ICON_FA_PLAY;
        else
            pauseLbl = ICON_FA_PAUSE;
        if(ImGui::Button(pauseLbl.c_str()))
            paused_ = !paused_;
        if(!paused_)
            ImGui::BeginDisabled();
        ImGui::SameLine();
        if(ImGui::Button(ICON_FA_FORWARD_STEP))
            ++step_;
        if(!paused_)
            ImGui::EndDisabled();
    }
    ImGui::End();
}

void Debug::DrawColorPicker()
{
    ImGuiColorEditFlags colorFlags = ImGuiColorEditFlags_AlphaPreview;
    ImGui::Text("Palette");
    for(int i = 0; i < Color::PaletteSize; ++i)
    {
        if(i % 8 != 0)
            ImGui::SameLine();

        if(ImGui::ColorButton(("##ColorPickerBtn" + std::to_string(i)).c_str(), Color::RlToIm(Color::Palette[i]),
                              colorFlags, ImVec2(20, 20)))
        {
            if(updatedColor_ != nullptr)
                *updatedColor_ = Color::Palette[i];
        }
    }

    ImGui::Separator();
    ImGui::Text("Color Picker");
    ImVec4 imColor = Color::RlToIm(*updatedColor_);
    ImGui::ColorPicker4("##ColorPicker", (float*)&imColor,
                        colorFlags | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);
    *updatedColor_ = Color::ImToRl(imColor);
}

#endif
