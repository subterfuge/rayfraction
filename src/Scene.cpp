#include "Scene.h"

#include "Debug.h"

IScene* Scene::SwitchScene()
{
    return nullptr;
}

void Scene::Init()
{}

void Scene::Draw()
{}

void Scene::Update(const double& dt)
{
    dt; // Removing warning C4100 on MSVC
}

#ifdef DEBUG_TOOLS

void Scene::DebugDraw()
{
    Debug::Draw();
}

#endif
