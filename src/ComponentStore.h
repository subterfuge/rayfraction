/**
 * Inspired by http://Entity-systems.wikidot.com/test-for-parallel-processing-of-components#cpp
 * and Jeffery Myers https://github.com/JeffM2501/raylib_ecs_sample/blob/main/test/components.h
 */

#pragma once

#include <map>
#include <vector>
#include <typeindex>
#include <functional>
#include "Alias.h"

class Component;

template<typename ComponentT>
using ComponentTable = std::map<EntityId, std::vector<ComponentT*>>;

using ComponentTableBase = ComponentTable<Component>;

namespace ComponentStore
{
    void Store(const ComponentId& componentTypeId, Component* component);

    std::map<ComponentId, ComponentTableBase>& GetDatabase();

    ComponentId NextId();

    std::vector<Component*> AllInEntity(const EntityId& entity);

    template<typename ComponentT, typename... Args>
    inline ComponentT* Build(const EntityId& entity, Args&&... args)
    {
        auto* component = new ComponentT(entity, std::forward<Args>(args)...);
        Store(ComponentT::GetBaseTypeId(), component);
        return component;
    }

    template<typename ComponentT>
    inline bool Destroy(const EntityId& entity)
    {
        delete Get<ComponentT>(entity);
    }

    template<typename ComponentT>
    inline ComponentT* Get(const EntityId& entity)
    {
        ComponentId id = ComponentT::GetBaseTypeId();
        auto database = GetDatabase();
        auto tableIt = database.find(id);
        if(tableIt == database.end())
            return nullptr;

        auto table = tableIt->second;
        auto componentListIt = table.find(entity);
        if(componentListIt == table.end() || componentListIt->second.empty())
            return nullptr;

        return static_cast<ComponentT*>(componentListIt->second[0]);
    }

    template<typename ComponentT>
    inline std::vector<Component*> All(const EntityId& entity)
    {
        ComponentId id = ComponentT::GetBaseTypeId();
        auto database = GetDatabase();
        auto tableIt = database.find(id);
        if(tableIt == database.end())
            return std::vector<Component*>();

        auto table = tableIt->second;
        auto componentListIt = table.find(entity);
        if(componentListIt == table.end() || componentListIt->second.empty())
            return std::vector<Component*>();

        return componentListIt->second;
    }

    template<typename ComponentT>
    ComponentTableBase& All()
    {
        return GetDatabase()[ComponentT::GetBaseTypeId()];
    }

    template<typename ComponentT>
    void ForEach(const std::function<void(ComponentT*)>& function)
    {
        ComponentTableBase& result = All<ComponentT>();
        for(const auto& entityIt : result)
        {
            for(auto* componentIt : entityIt.second)
                function(static_cast<ComponentT*>(componentIt));
        }
    }

    void DeleteAll(EntityId entity);

    void EndDelete(EntityId entity, Component* component);
};
