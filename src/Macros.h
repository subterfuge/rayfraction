#pragma once

#define FLAG(Type)                                                  \
inline static Type operator&(const Type& lhs, const Type& rhs) {    \
    return static_cast<Type>(                                       \
    static_cast<std::underlying_type_t<Type>>(lhs) &                \
    static_cast<std::underlying_type_t<Type>>(rhs)                  \
    );                                                              \
}                                                                   \
                                                                    \
inline static Type operator^(const Type& lhs, const Type& rhs) {    \
    return static_cast<Type>(                                       \
    static_cast<std::underlying_type_t<Type>>(lhs) ^                \
    static_cast<std::underlying_type_t<Type>>(rhs)                  \
    );                                                              \
}                                                                   \
                                                                    \
inline static Type operator|(const Type& lhs, const Type& rhs) {    \
    return static_cast<Type>(                                       \
    static_cast<std::underlying_type_t<Type>>(lhs) |                \
    static_cast<std::underlying_type_t<Type>>(rhs)                  \
    );                                                              \
}                                                                   \
                                                                    \
inline static Type& operator&=(Type& lhs, const Type& rhs) {        \
    lhs = lhs & rhs;                                                \
    return lhs;                                                     \
}                                                                   \
                                                                    \
inline static Type& operator|=(Type& lhs, const Type& rhs) {        \
    lhs = lhs | rhs;                                                \
    return lhs;                                                     \
}                                                                   \
                                                                    \
inline static Type& operator^=(Type& lhs, const Type& rhs) {        \
    lhs = lhs ^ rhs;                                                \
    return lhs;                                                     \
}
