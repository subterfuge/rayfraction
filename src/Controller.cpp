#include "Controller.h"

#include "raylib_namespace.h"

ControllerSystem::ControllerSystem()
        : state_(new BaseControllerState())
{}

ControllerSystem::ControllerSystem(const ControllerSystem& controller)
        : state_(controller.state_->Clone())
{}


ControllerSystem::ControllerSystem(ControllerSystem&& controller) noexcept
        : state_(controller.state_)
{}

ControllerSystem& ControllerSystem::operator=(const ControllerSystem& controller)
{
    if(this == &controller)
        return *this;

    state_ = controller.state_->Clone();
    return *this;
}

ControllerSystem& ControllerSystem::operator=(ControllerSystem&& controller) noexcept
{
    if(this == &controller)
        return *this;

    delete state_;
    state_ = controller.state_;
    controller.state_ = nullptr;
    return *this;
}

ControllerSystem::~ControllerSystem()
{
    delete state_;
    state_ = nullptr;
}

void ControllerSystem::Update()
{
    if(rl::GetKeyPressed() == 0)
        return;

#ifdef DEBUG_TOOLS
    if((rl::IsKeyDown(rl::KEY_LEFT_CONTROL) || rl::IsKeyDown(rl::KEY_RIGHT_CONTROL)) &&
       rl::IsKeyPressed(rl::KEY_F1))
        state_->DebugMenu();
    else if(rl::IsKeyPressed(rl::KEY_F1))
        state_->DebugFps();
    else if(rl::IsKeyPressed(rl::KEY_F2))
        state_->DebugEntityList();
    else if(rl::IsKeyPressed(rl::KEY_F3))
        state_->DebugEntityExplorer();
    else if(rl::IsKeyPressed(rl::KEY_F4))
        state_->DebugControls();
    else if(rl::IsKeyPressed(rl::KEY_F5))
        state_->DebugImGuiDemo();
#endif
}

void ControllerSystem::SwitchState(IControllerState* newState)
{
    if(newState == nullptr)
        return;

    delete state_;
    state_ = newState;
}
