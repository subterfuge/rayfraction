#pragma once

#include <memory>
#include <map>
#include "Alias.h"
#include "Component.h"
#include "ComponentStore.h"

inline constexpr EntityId ENTITY_ID_NULL = 0;

namespace EntityBuilder
{
    EntityId Create(const std::string& name);
    void Delete(EntityId entity);
};
